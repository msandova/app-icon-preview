use super::icon::{Icon, IconSize};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

#[derive(Debug, Copy, PartialEq, Clone)]
pub enum PaneStyle {
    Light,
    Dark,
}

impl Default for PaneStyle {
    fn default() -> Self {
        Self::Light
    }
}

mod imp {
    use super::*;

    use std::cell::{Cell, RefCell};

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/gnome/design/AppIconPreview/colourpane.ui")]
    pub struct ColourPane {
        pub style: Cell<PaneStyle>,
        pub small_icons: RefCell<Vec<Icon>>,
        pub grid_icons: RefCell<Vec<Icon>>,

        #[template_child]
        pub symbolic_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub symbolic_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub hicolor_128: TemplateChild<gtk::Image>,
        #[template_child]
        pub hicolor_64: TemplateChild<gtk::Image>,
        #[template_child]
        pub hicolor_32: TemplateChild<gtk::Image>,
        #[template_child]
        pub grid: TemplateChild<gtk::Box>,
        #[template_child]
        pub small: TemplateChild<gtk::Box>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColourPane {
        const NAME: &'static str = "ColourPane";
        type Type = super::ColourPane;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ColourPane {}
    impl WidgetImpl for ColourPane {}
    impl BoxImpl for ColourPane {}
}

glib::wrapper! {
    pub struct ColourPane(ObjectSubclass<imp::ColourPane>)
        @extends gtk::Widget, gtk::Box;
}

impl ColourPane {
    pub fn new(style: PaneStyle) -> Self {
        let pane = glib::Object::new::<Self>(&[]).unwrap();
        let self_ = imp::ColourPane::from_instance(&pane);
        self_.style.set(style);
        pane.init();
        pane
    }

    pub fn set_hicolor(&self, icon_name: &str) {
        let self_ = imp::ColourPane::from_instance(self);

        if let Some(icon) = self_.small_icons.borrow_mut().get(2) {
            icon.set_icon_name(icon_name);
        }
        if let Some(icon) = self_.grid_icons.borrow_mut().get(1) {
            icon.set_icon_name(icon_name);
        }

        self_.hicolor_128.set_icon_name(Some(icon_name));
        self_.hicolor_64.set_icon_name(Some(icon_name));
        self_.hicolor_32.set_icon_name(Some(icon_name));
    }

    pub fn set_symbolic(&self, basename: Option<&str>) {
        let self_ = imp::ColourPane::from_instance(self);

        match basename {
            Some(basename) => {
                let icon_name = format!("{}-symbolic", basename.trim_end_matches(".svg"));
                self_.symbolic_image.set_icon_name(Some(&icon_name));
                self_.symbolic_image.show();
                self_.symbolic_label.show();
            }
            None => {
                self_.symbolic_image.hide();
                self_.symbolic_label.hide();
            }
        }
    }

    pub fn load_samples(&self, samples: &[gio::File]) {
        let self_ = imp::ColourPane::from_instance(self);
        // We fill the small icons
        assert_eq!(samples.len(), 6);
        let mut sample_idx = 0;
        for i in 0..5 {
            if i == 2 {
                continue;
            }
            let file = samples.get(sample_idx).unwrap();
            if let Some(icon) = self_.small_icons.borrow_mut().get(i) {
                icon.set_file(file);
            }
            sample_idx += 1;
        }
        // Then the grid ones
        let mut sample_idx = 4;
        for i in 0..3 {
            if i == 1 {
                continue;
            }
            let file = samples.get(sample_idx).unwrap();
            if let Some(icon) = self_.grid_icons.borrow_mut().get(i) {
                icon.set_file(file);
            }
            sample_idx += 1;
        }
    }

    fn init(&self) {
        let self_ = imp::ColourPane::from_instance(self);
        match self_.style.get() {
            PaneStyle::Dark => {
                self.add_css_class("dark");
            }
            PaneStyle::Light => {
                self.add_css_class("light");
            }
        };

        // Small container is composed of 5 icons, 4 samples & the previewed project
        let small_group = gtk::SizeGroup::new(gtk::SizeGroupMode::Horizontal);
        for _ in 0..5 {
            let demo_icon = Icon::new(IconSize::Small);
            small_group.add_widget(&demo_icon);
            self_.small.append(&demo_icon);
            self_.small_icons.borrow_mut().push(demo_icon);
        }

        // Grid container is composed of 3 icons, 2 samples & the previewed project
        let grid_group = gtk::SizeGroup::new(gtk::SizeGroupMode::Horizontal);
        for _ in 0..3 {
            let demo_icon = Icon::new(IconSize::Large);
            grid_group.add_widget(&demo_icon);
            self_.grid.append(&demo_icon);
            self_.grid_icons.borrow_mut().push(demo_icon);
        }
    }
}
